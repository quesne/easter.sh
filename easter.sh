#!/bin/bash

Y=$1

if [[ "x${Y}" = "x" ]]; then Y=`date +%Y`;fi

#######Easter-fu
C=$(echo "scale=0;$Y/100"|bc)
L=$(echo "scale=0;$Y%19"|bc)
K=$(echo "scale=0;($C-17)/25"|bc)
A=$(echo "scale=0;($C/4)"|bc)
B=$(echo "scale=0;(($C-$K)/3)"|bc)
D=$(echo "scale=0;(15+$C-$A-$B+19*$L)%30"|bc)
F=$(echo "scale=0;($L/11)"|bc)
G=$(echo "scale=0;(($D+$F)/29)"|bc)
R=$(echo "scale=0;$D-$G"|bc)
H=$(echo "scale=0;$Y/4"|bc)
J=$(echo "scale=0;($Y+$H+$R+2-$C+$A)%7"|bc)
E=$(echo "scale=0;(28+$R-$J)"|bc)
if [ $E -gt 31 ]
then
 let "E = $E - 31"
 MONTH=4
else
 MONTH=3
fi
#######

DAY=`printf "%02d\n" $E`
MONTH=`printf "%02d\n" $MONTH`

echo "Easter Sunday for $Y is `date '+%A %B %-d' -d"${Y}${MONTH}${DAY}"`"